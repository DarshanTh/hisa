/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|                                                                             |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSchemes;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Shock capturing schemes (AUSMPlusUp and HLLC)
fluxScheme          AUSMPlusUp;

// Time stepping (Second order (beta = 1) and first order implicit (beta = 0))
ddtSchemes
{
    default         dualTime rPseudoDeltaT CrankNicolson 0.9; //  0 -> Euler implicit, 1 -> CN
}

gradSchemes
{
    default         faceLeastSquares linear;
}

divSchemes
{
}

laplacianSchemes
{
}

interpolationSchemes
{
    default         linear;
    reconstruct(rho) wVanLeer;
    reconstruct(U)  wVanLeer;
    reconstruct(T)  wVanLeer;
}

snGradSchemes
{
}


// ************************************************************************* //
