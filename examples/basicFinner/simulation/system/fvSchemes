/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSchemes;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


fluxScheme           AUSMPlusUp;
lowMachAusm          false;

ddtSchemes
{
    default          bounded dualTime rPseudoDeltaT steadyState;
}

gradSchemes
{
    default          faceLeastSquares linear;
    gradTVD          faceLeastSquares linear;
    grad(nuTilda)    cellLimited Gauss linear 0.9;
    grad(k)          cellLimited Gauss linear 0.9;
    grad(omega)      cellLimited Gauss linear 0.9;
}

divSchemes
{
    default          none;
    div(tauMC)       Gauss linear;
    div(phi,nuTilda) bounded Gauss limitedLinear 1;
    div(phi,k)       bounded Gauss limitedLinear 1;
    div(phi,omega)   bounded Gauss limitedLinear 1;
}

laplacianSchemes
{
    default                     Gauss linear corrected;
    laplacian(muEff,U)          Gauss linear compact;
    laplacian(alphaEff,e)       Gauss linear compact;
}

interpolationSchemes
{
    default          linear;
    reconstruct(rho) wVanLeer gradTVD;
    reconstruct(U)   wVanLeer gradTVD;
    reconstruct(T)   wVanLeer gradTVD;
}

snGradSchemes
{
    default          corrected;
}

wallDist
{
    method           Poisson;
}


// ************************************************************************* //
